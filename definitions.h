
#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#define NUM 1000
#define FUNCTION_UNDEFINED -9999.0
#define PI 3.14159265358979
#define E 2.71828182845904
#define epsilon 0.01
#define MAX_DEPTH 80
//#define MAX_TOKENS 4
#define SCREEN_WIDTH 1000
#define TRUE 1
#define FALSE 0

typedef unsigned char boolean;


typedef struct{
    int top; 
    char array[MAX_DEPTH];
} Stack;


typedef struct{
	int top;
	float array[MAX_DEPTH];
} NumericStack;

/* math_plot.c */
LRESULT CALLBACK WndProc (HWND, UINT, WPARAM, LPARAM);
boolean parse_user_input(char *text, char *func_name);
void build_function(char *postfix_expression, POINT *func_line, int max_x, int max_y);

/* functions.h */
int neg(int number);
int factorial(int x);
double absolute(double x);
float exponential(float x);
float ln(float x);
float lb(float x);
float lg(float x);
float logarithm(float x, float base);
float power(float x, float exponent);
float root(float x, int n);
float square_root(float x);
float sine(float x);
float cosine(float x);
float tangent(float x);
float cotangent(float x);
float arcsine(float x);
float arccosine(float x);
float arctangent(float x);
float arccotangent(float x);
float secant(float x);
float cosecant(float x);
float sine_hyperbolic(float x);
float cosine_hyperbolic(float x);
float tangent_hyperbolic(float x);
float cotangent_hyperbolic(float x);

/* shunting_yard.h */
Stack* create_stack(void);
NumericStack* create_numstack(void);
void destroy_stack(Stack* stack);
boolean is_empty(Stack* stack);
char peek(Stack* stack);
void push(Stack* stack, char value);
char pop(Stack* stack);
int peek_num(NumericStack* stack);
void push_num(NumericStack* stack, float value);
float pop_num(NumericStack* stack);
boolean is_operand(char *token);
int precedence(char token);
void append_popped_string(Stack* stack, char* queue);
void handle_operator(Stack *stack, char* queue, char token);
char* infix_to_postfix(char* expression, size_t len);
boolean illegal_expression(char* postfix_string);
float calculate (char* postfix_string, float x);

/* logging.h */
void write_log(char* message, size_t level);

#endif
