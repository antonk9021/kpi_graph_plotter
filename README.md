# MATH_PLOT

## This is a README for the `math_plot` utility

### System requirements
* Windows XP/7/8/8.1/10
* C99 compiler

### Installation

1. Compile `math_plot.c` with the following flags:
+ -std=C99 -lgdi32 -mwindows

2. Launch `math_plot.exe`