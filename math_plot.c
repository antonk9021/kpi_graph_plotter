
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <windows.h>
#include "definitions.h"
#include "logging.h"
#include "functions.h"
#include "shunting_yard.h"


int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int nCmdShow) {
	static TCHAR szAppName[] = TEXT("������ �������");
	HWND mainWindow;
	MSG msg;
	WNDCLASSEX wc;

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH) GetStockObject (WHITE_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = szAppName;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if (!RegisterClassEx(&wc)) {
		MessageBox (NULL, TEXT("³������ ��� ��������� ����� ����. "
		                       "�������� ������������ ���� �������� ���������."),
		            "�������", MB_ICONERROR);
		write_log("Window class registration failure\n", 0);
		return 0;
	}

	mainWindow = CreateWindow(szAppName, TEXT("�������� ������� �������"),
	                          WS_OVERLAPPEDWINDOW | WS_HSCROLL,
	                          CW_USEDEFAULT, CW_USEDEFAULT,
	                          CW_USEDEFAULT, CW_USEDEFAULT,
	                          NULL, NULL, hInstance, NULL);

	ShowWindow(mainWindow, nCmdShow);
	UpdateWindow (mainWindow);

	while (GetMessage (&msg, NULL, 0, 0)) {
		TranslateMessage (&msg);
		DispatchMessage (&msg);
	}
	return msg.wParam;
}

LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	static int max_x, max_y;
	static size_t len;
	static unsigned char draw = 0;
	char *user_input;
	HDC hdc;
//	HDC hdc_func_line;
	PAINTSTRUCT ps;
	static POINT line[SCREEN_WIDTH];
	static HWND text_box;
	static HWND send_button;
	static char * math_func;
	static char * inverse;

	switch (message) {
		case WM_CREATE:
			text_box = CreateWindow("EDIT",
			                        "",
			                        WS_BORDER | WS_CHILD | WS_VISIBLE,
			                        10, 5, 390, 20,
			                        hwnd, (HMENU) 0, NULL, NULL);
			send_button = CreateWindow("BUTTON",
			                           "���������� ������",
			                           WS_VISIBLE | WS_CHILD | WS_BORDER,
			                           420, 5, 165, 20,
			                           hwnd, (HMENU) 1, NULL, NULL);
			break;

		case WM_COMMAND:
			switch(LOWORD(wParam)) {
				case 1:
					len = GetWindowTextLength(text_box) + 1;
					user_input = malloc(sizeof(char)*len);
					math_func = malloc(sizeof(char)*len*2);
					inverse = malloc(sizeof(char)*len);
					GetWindowText(text_box, &user_input[0], len);
					draw = parse_user_input(user_input, math_func);
					inverse = infix_to_postfix(math_func, len);
					if (draw) {
						RedrawWindow(hwnd, NULL, NULL, RDW_ERASE | RDW_INVALIDATE);
					}
					free(user_input);
					free(math_func);
					break;
			}
			break;

		case WM_SIZE:
			max_x = LOWORD (lParam);
			max_y = HIWORD (lParam);
			return 0;



		case WM_PAINT:
			/* Coordinates system */
			hdc = BeginPaint(hwnd, &ps);
			MoveToEx(hdc, 0, max_y/2, NULL);
			LineTo(hdc, max_x, max_y/2);
			MoveToEx(hdc, max_x/2, 0, NULL);
			LineTo(hdc, max_x/2, max_y);

			if (draw) {
				MoveToEx(hdc, 0, max_y/2, NULL);
				LineTo(hdc, max_x, max_y/2);
				MoveToEx(hdc, max_x/2, 0, NULL);
				LineTo(hdc, max_x/2, max_y);
				if (illegal_expression(inverse)){
					MessageBox (NULL, TEXT("�� ������� �������� �������. "
		                       "�������� ������������ �� �����."),
		            			"�������", MB_ICONERROR);
				}
				build_function(inverse, line, max_x, max_y);
				for (int i = 0; i < NUM-1; i++) {
					if ((line[i].y != FUNCTION_UNDEFINED) && (line[i + 1].y != FUNCTION_UNDEFINED)) {
						MoveToEx(hdc, line[i].x, line[i].y, &line[i]);
						LineTo(hdc, line[i+1].x, line[i+1].y);
					}
				}
				
				//Polyline(hds, line, NUM); 
				
			}
			EndPaint(hwnd, &ps);
			return 0;

		case WM_DESTROY:
			PostQuitMessage (0);
			return 0;
	}
	return DefWindowProc (hwnd, message, wParam, lParam);
}


boolean parse_user_input(char *text, char *func_name) {
	int i = 0;
	int j = 0;
	while (text[i] != '\0') {
		if (isspace(text[i]) || text[i] == '\r') {
			i++;
			continue;
		}
		func_name[j] = text[i];
		if (!((isalpha(text[i]) && isalpha(text[i+1])) 
			|| (isdigit(text[i]) && isdigit(text[i+1]) 
			|| (isdigit(text[i]) && text[i+1] == '.')
			|| text[i] == '.' && (isdigit(text[i+1])))
			|| text[i+1] == '\0'
			|| text[i+1] == '\n'
			|| text[i+1] == '\r')) {
			func_name[++j] = ' ';
		}
		i++;
		j++;
	}
	func_name[j] = '\0';
//	printf("%s\n", func_name);
	return TRUE;
}

void build_function(char *postfix_expression, POINT *func_line, int max_x, int max_y) {
	for (int x=(int) -NUM/2; x < (int) NUM/2; x++) {
		int index = x+(int) NUM/2;
		func_line[index].x = (int) (index*max_x/NUM);
		float y = calculate(postfix_expression, 8.0 * (float)x / NUM);
		if (y == FUNCTION_UNDEFINED) {
			func_line[index].y = FUNCTION_UNDEFINED;
		}else{
			func_line[index].y = (int)(max_y / 4 * (2 - y));
		}
	}
}

