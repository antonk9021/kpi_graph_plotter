#ifndef SHUNTING_YARD_H
#define SHUNTING_YARD_H


Stack* create_stack(void) {
	Stack* stack = malloc(sizeof(Stack));
	if (!stack) {
		write_log("Stack creation failed\n", 0);
		return NULL;
	}
	stack->top = -1;

	return stack;
}


NumericStack* create_numstack(void) {
	NumericStack* stack = malloc(sizeof(NumericStack));
	if (!stack) {
		write_log("Numeric stack creation failed\n", 0);
		return NULL;
	}
	stack->top = 0;

	return stack;
}


void destroy_stack(Stack* stack) {
	free(stack);
}


boolean is_empty(Stack* stack) {
	return stack->top == -1;
}


char peek(Stack* stack) {
	return stack->array[stack->top];
}

void push(Stack* stack, char value) {
	stack->array[++stack->top] = value;
}


char pop(Stack* stack) {
	if (!is_empty(stack)) {
		return stack->array[stack->top--];
	}
	return '\0';
}


int peek_num(NumericStack* stack) {
	return stack->array[stack->top];
}


void push_num(NumericStack* stack, float value) {
	/*stack->top++;
	stack->array[stack->top] = value;*/
	stack->array[++stack->top] = value;
}


float pop_num(NumericStack* stack) {
	if (stack->top != 0) {
		return stack->array[stack->top--];
	}
	return .0;
}

boolean is_operand(char *token) {
	if (!strcmp(token, "exp")) {
		return FALSE;
	}
	return (isdigit(*token) || tolower(*token) == 'x' || tolower(*token) == 'e' || !strcmp(token, "pi"));
}


int precedence(char token) {
	switch(tolower(token)) {
		case '+':
		case '-':
			return 1;
		case '*':
		case '/':
			return 2;
		case '^':
			return 3;
		case 'a': // abs
		case 's': // sin/sh
		case 'c': // cos/ch
		case 't': // tg/th
		case 'g': // ctg/cth
		case 'k': //sec
		case 'y': //cosec
		case 'i': // arcsin
		case 'o': // arccos/log
		case 'r': // arctg
		case 'm': // arcctg
		case 'p': // exp
		case 'q': // sqrt
		case 'l': // ln
		case 'd': // lg
		case 'b': // lb
			return 4;
		default:
			return -1;
	}
}


void append_popped_string(Stack* stack, char* queue) {
	char popped = pop(stack);
	char *popped_string = malloc(3);
	popped_string[2] = '\0'; // in order to make strcat() working
	popped_string[1] = '\\';
	popped_string[0] = popped;
	strcat(queue, popped_string);
	free(popped_string);
}


void handle_operator(Stack *stack, char* queue, char token) {
	while(!is_empty(stack) && precedence(token) <= precedence(peek(stack))) {
		append_popped_string(stack, queue);
	}
	push(stack, token);
}


char* infix_to_postfix(char* expression, size_t len) {
	int i = 0;
	size_t operands_counter = 0;
	//char current_top;
	//char * current_top_pointer;
	char * queue;
	queue = malloc(len);
	queue[0] = '\0';

	Stack* stack = create_stack();
	if(!stack) {
		write_log("Stack creation failed, terminating\n", 0);
		exit(-1);
	}

	char *token = strtok(expression, ", ");

	while (token != '\0') {
		if (is_operand(token)) {
			strcat(queue, token);
			strcat(queue, "\\");
			operands_counter++;
		} else if (!strcmp(token, "abs")) {
			handle_operator(stack, queue, 'a');
		} else if (!strcmp(token, "exp")) {
			handle_operator(stack, queue, 'p');
		} else if (!strcmp(token, "sqrt")) {
			handle_operator(stack, queue, 'q');
		} else if (!strcmp(token, "sin")) {
			handle_operator(stack, queue, 's');
		} else if (!strcmp(token, "cos")) {
			handle_operator(stack, queue, 'c');
		} else if ((!strcmp(token, "tan")) || (!strcmp(token, "tg"))) {
			handle_operator(stack, queue, 't');
		} else if ((!strcmp(token, "cot")) || (!strcmp(token, "ctg"))) {
			handle_operator(stack, queue, 'g');
		} else if (!strcmp(token, "sec")) {
			handle_operator(stack, queue, 'k');
		} else if (!strcmp(token, "cosec")) {
			handle_operator(stack, queue, 'y');
		} else if (!strcmp(token, "sh") || !strcmp(token, "sinh")) {
			handle_operator(stack, queue, 'S');
		} else if (!strcmp(token, "ch") || !strcmp(token, "cosh")) {
			handle_operator(stack, queue, 'C');
		} else if (!strcmp(token, "th") || !strcmp(token, "tanh")) {
			handle_operator(stack, queue, 'T');
		} else if (!strcmp(token, "cth") || !strcmp(token, "coth")) {
			handle_operator(stack, queue, 'G');
		} else if (!strcmp(token, "arcsin")) {
			handle_operator(stack, queue, 'i');
		} else if (!strcmp(token, "arccos")) {
			handle_operator(stack, queue, 'w');
		} else if (!strcmp(token, "ln")) {
			handle_operator(stack, queue, 'l');
		} else if (!strcmp(token, "lg")) {
			handle_operator(stack, queue, 'd');
		} else if (!strcmp(token, "lb")) {
			handle_operator(stack, queue, 'b');
		} else if (!strcmp(token, "log")) {
			handle_operator(stack, queue, 'O');
		} else if (!strcmp(token, "root")){
			handle_operator(stack, queue, 'R');
		} else if (*token == '(') {
			push(stack, *token);
		} else if (*token == ')') {
			while(!is_empty(stack) && !(peek(stack) == '(')) {
				append_popped_string(stack, queue);
			}
			if (!is_empty(stack) && !(peek(stack) == '(')) {
				write_log("Stack error: check brackets\n", 1);
				return "ERROR";
			} else {
				pop(stack);
			}
		} else {
			handle_operator(stack, queue, *token);
//			switch(*token){
//				case '+':
//				case '-':
//				case '*':
//				case '/':
//				case '^':
//					handle_operator(stack, queue, *token);
//					break;
//				case '\0':
//					break;
//				default:
//					return "ERROR";
//			}
//
		}
		i++;
		token = strtok(NULL, ", ");
	}

	while (!is_empty(stack)) {
		append_popped_string(stack, queue);
	}

//    destroy_stack(stack);
	if (!operands_counter){
		return "ERROR";
	}
	
	return queue;
}


boolean illegal_expression(char* postfix_string) {
	if (!strcmp(postfix_string, "ERROR")) {
		return TRUE;
	}
	return FALSE;
}


float calculate (char* postfix_string, float x) {
	NumericStack* stack = create_numstack();
	static float result = .0;
	float second_operand;
	char *expression_to_process = malloc(strlen(postfix_string));
	//const size_t expression_length = strlen(postfix_string);
	
	//char expression_to_process[sizeof(postfix_string)+2];

	strcpy(expression_to_process, postfix_string);
//	printf("p: %s %d\ne: %s %d\n", postfix_string, strlen(postfix_string), expression_to_process, strlen(expression_to_process));
	char *token = strtok(expression_to_process, "\\");
	while (token != '\0') {
		if (is_operand(token)) {
			if (tolower(*token) == 'x') {
				result = x;
				push_num(stack, x);
			} else if (tolower(*token) == 'e') {
				result = E;
				push_num(stack, E);
			} else if (!strcmp(token, "pi")) {
				result = PI;
				push_num(stack, PI);
			} else {
				result = atof(token);
				push_num(stack, atof(token));
			}
		} else {
			switch(*token) {
				case '+':
					result = pop_num(stack);
					second_operand = pop_num(stack);
					if (result == FUNCTION_UNDEFINED || second_operand == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result += second_operand;
					push_num(stack, result);
					break;
				case '-':
//					if (peek_num(stack) == 0) {
//						result = pop_num(stack);
//						if (result == FUNCTION_UNDEFINED) {
//							push_num(stack, result);
//							break;
//						}
//						result = -result;
//					} else {
						second_operand = pop_num(stack);
						result = pop_num(stack);
						if (result == FUNCTION_UNDEFINED || second_operand == FUNCTION_UNDEFINED) {
							push_num(stack, result);
							break;
						}
						result -= second_operand;
//						printf("%.2f\n", result);
//					}
					push_num(stack, result);
					break;
				case '*':
					result = pop_num(stack);
					second_operand = pop_num(stack);
					result *= second_operand;
					if (result == FUNCTION_UNDEFINED || second_operand == FUNCTION_UNDEFINED) {
							push_num(stack, result);
							break;
						}
					push_num(stack, result);
					break;
				case '/':
					second_operand = pop_num(stack);
					result = pop_num(stack);
					if (second_operand != .0) {
						if (result == FUNCTION_UNDEFINED || second_operand == FUNCTION_UNDEFINED) {
							push_num(stack, result);
							break;
						}
						result /= second_operand;
					} else {
						push_num(stack, FUNCTION_UNDEFINED);
						break;
					}
					push_num(stack, result);
					break;
				case '^':
					second_operand = pop_num(stack);
					result = pop_num(stack);
					/*if (!second_operand) {
						result = 1.0;
					} else {
						result = power(result, second_operand);
					}*/
					if (result == FUNCTION_UNDEFINED || second_operand == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = power(result, second_operand);
					push_num(stack, result);
					break;
				case 'a': // abs
					result = pop_num(stack);
					if (result == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = absolute(result);
					push_num(stack, result);
					break;
				case 'p': // exp
					result = pop_num(stack);
					if (result == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = exponential(result);
					push_num(stack, result);
					break;
				case 'q': //square root
					result = pop_num(stack);
					if (result == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = square_root(result);
					push_num(stack, result);
					break;
				case 'R': // arbitrary root
					second_operand = pop_num(stack);
					result = pop_num(stack);
					if (result == FUNCTION_UNDEFINED || second_operand == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = root(result, second_operand);
					push_num(stack, result);
					break;
				case 's': // sine
					result = pop_num(stack);
					if (result == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = sine(result);
					push_num(stack, result);
					break;
				case 'c': // cosine
					result = pop_num(stack);
					if (result == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = cosine(result);
					push_num(stack, result);
					break;
				case 't': // tangent
					result = pop_num(stack);
					if (result == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = tangent(result);
					push_num(stack, result);
					break;
				case 'g': // cotangent
					result = pop_num(stack);
					if (result == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = cotangent(result);
					push_num(stack, result);
					break;
				case 'k': // secant
					result = pop_num(stack);
					if (result == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = secant(result);
					push_num(stack, result);
					break;
				case 'y': // cosecant
					result = pop_num(stack);
					if (result == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = cosecant(result);
					push_num(stack, result);
					break;
				case 'i': // arcsin
					result = pop_num(stack);
					if (result == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = arcsine(result);
					push_num(stack, result);
					break;
				case 'w': // arccos
					result = pop_num(stack);
					if (result == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = arccosine(result);
					push_num(stack, result);
					break;
				case 'l': // ln
					result = pop_num(stack);
					if (result == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = ln(result);
					push_num(stack, result);
					break;
				case 'd': // lg
					result = pop_num(stack);
					if (result == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = lg(result);
					push_num(stack, result);
					break;
				case 'b': // lb
					result = pop_num(stack);
					if (result == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = lb(result);
					push_num(stack, result);
					break;
				case 'O': // log
					result = pop_num(stack);
					second_operand = pop_num(stack);
					if (result == FUNCTION_UNDEFINED || second_operand == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = logarithm(result, second_operand);
					push_num(stack, result);
					break;
				case 'S': // sh
					result = pop_num(stack);
					if (result == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = sine_hyperbolic(result);
					push_num(stack, result);
					break;
				case 'C': // ch
					result = pop_num(stack);
					if (result == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = cosine_hyperbolic(result);
					push_num(stack, result);
					break;
				case 'T': // th
					result = pop_num(stack);
					if (result == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = tangent_hyperbolic(result);
					push_num(stack, result);
					break;
				case 'G': // cth
					result = pop_num(stack);
					if (result == FUNCTION_UNDEFINED) {
						push_num(stack, result);
						break;
					}
					result = cotangent_hyperbolic(result);
					push_num(stack, result);
					break;
				default:
					break;
			}
		}
		token = strtok(NULL, "\\");
	}


	//free(expression_to_process);
//	free(stack);


	return result;
}

#endif

