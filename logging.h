
void write_log(char* message, size_t level) {
	if (level > 0) {
		printf("%s\n", message);
	}

	FILE *fptr;

	fptr = fopen("log.txt", "a");
	fprintf(fptr, "%s\n", message);
	fclose(fptr);
}
