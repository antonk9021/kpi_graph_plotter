
#ifndef FUNCTIONS_H
#define FUNCTIONS_H


//int neg(int number) {
//	return -number;
//}


int factorial(int x) {
	int result = 1;

	if (x == 0) {
		return 1;
	}

	for (int i=1; i<=x; i++) {
		result *= i;
	};

	return result;
}


double absolute(double x) {
	return x < .0 ? -x : x;
}

float exponential(float x) {
	if (x == .0) {
		return 1.0;
	}
	double step = 1.0;
	double taylor_sum = 1.0;
	int n = 0;

	while(absolute(step) > epsilon/1000) {
		step *= x/(double)(n+1);
		taylor_sum += step;
		n++;
	}

	return taylor_sum;
}


float ln(float x) {
	/* Mercator's series in Gregory's modification */

	/* http://mathemlib.ru/books/item/f00/s00/z0000027/st226.shtml */

	if (x < 0.0) {
		return FUNCTION_UNDEFINED;
	}

	float shifted_x = (x-1.0)/(x+1.0);

	float step = shifted_x;
	float sum = shifted_x;
	int n = 0;

	while (absolute(step) > epsilon/1000) {
		step *= (shifted_x*shifted_x*(2*n + 1))/(2*n + 3);
		sum += step;
		n++;
	}

//	printf("ln(%.4f): %.4f\n", x, 2*sum);
	return 2*sum;
}


float modulo(float nominator, float denominator) {
	return (nominator - denominator*(int)(nominator/denominator));
}


float power(float x, float n) {
	
	if(x == E){
		return exponential(x);
	}

	if (x == .0 && n != .0) {
		return .0;
	}

	if (n == .0) {
		return 1.0;
	}
	
	float result = 1.0;
	
	/* HERE WE GO */

	if ((absolute(n - (int)n) != 0)) {
		if (x < 0) {
//			printf("x is negative: %.2f, %.2f\n", x, exponent);
			return FUNCTION_UNDEFINED;
		}
		/* a^b = e^(b*ln(a)) */
		return (exponential(n*ln(x)));
	}
	
//	if (x == 2.0) {
//		return exponent>0 ? 1 << (int)exponent : 1 / (1 << (int)exponent);
//	}
	

	for (int i=0; i < absolute(n); i++) {
		result *= x;
	}

	if (n < .0) {
		return 1.0 / result;
	}

	return result;
}


float root(float x, int n) {
	if ((x < .0) && !(n % 2)) { 
		return FUNCTION_UNDEFINED;
	}
	
	if (x == 2.0){
		return square_root(x);
	}

	float step = x;
	float kek;  // TODO: GET RID OF THIS STUPID `KEK`

	for (int i=0; i<50; i++) {
		kek = power(step, n-1);  
		step = (1.0/(float)n) * (((float)n-1.0)*step + x/kek);
	}

	return step;
}


float square_root(float x) {
	// Geron's formula

	if (x < .0) {
		return FUNCTION_UNDEFINED;
	}

	float step = x;
	for (int i=0; i<50; i++) {
		step = (float)0.5*(step + x/step);
	}

	return step;
}


float sine(float x) {
	if (x == 0.0) {
		return 0.0;
	}

	float step = x;
	if (absolute(x) >= 2*PI){
		x = modulo(x, 2*PI);
	}
	float taylor_sum = x;
	int n = 0;

	while(absolute(step) > epsilon) {
		step *= (-(x*x) / ((2*n + 2) * (2*n + 3)));
		taylor_sum += step;
		n++;
	}

	return taylor_sum;
}


float cosine(float x) {
	float step = 1.0;
	float taylor_sum = 1.0;
	int n = 0;

	while(absolute(step) > epsilon) {
		step *= (-(x*x) / ((2*n + 1) * (2*n + 2)));
		taylor_sum += step;
		n++;
	}

	return taylor_sum;
}


float tangent(float x) {
	//TODO: deal with this issue (the same for cotangent)
	if (x == .0 || absolute(cosine(x))-.01 < .0) {
		return FUNCTION_UNDEFINED;
	}

	return sine(x) / cosine(x);
}

float cotangent(float x) {
	if (x == .0 ||  absolute(sine(x))-0.01 < 0) {
		return FUNCTION_UNDEFINED;
	}
	return cosine(x) / sine(x);
}


float arcsine(float x) {
	if (absolute(x) > 1.0) {
		return FUNCTION_UNDEFINED;
	}

	float step = x;
	float taylor_sum = x;
	size_t n = 0;

	while(absolute(step) > epsilon) {
		/* No check needed since n cannot be negative */
		step *= (x*x*(2*n + 1)*(2*n + 1)*(2*n + 2))/(4*(n + 1)*(n + 1)*(2*n + 3));
		taylor_sum += step;
		n++;
	}

	return taylor_sum;
}


float arccosine(float x) {
	if (absolute(x) > 1.0) {
		return FUNCTION_UNDEFINED;
	}
	return PI / 2 - arcsine(x);
}


float secant(float x) {
	if (sine(x) == .0) {
		return .0;
	}
	return 1.0 / sine(x);
}


float cosecant(float x) {
	if (cosine(x) == .0) {
		return .0;
	}
	return 1.0 / cosine(x);
}


float lg(float x) {
	/* http://scask.ru/f_book_p_math2.php?id=81 */
	return 0.434294 * ln(x);
}


float lb(float x) {
	return ln(x)/0.693147; // ln 2
}


float logarithm(float x, float base) {
	/* FIXME: operates improperly */
	return ln(x)/ln(base);
}


float sine_hyperbolic(float x){
	return (exponential(x) - exponential(-x))/2.0;
}


float cosine_hyperbolic(float x){
	return (exponential(x) + exponential(-x))/2.0;
}


float tangent_hyperbolic(float x){
	return (exponential(2*x) - 1.0)/(exponential(2*x) + 1.0);
}


float cotangent_hyperbolic(float x){
	return (exponential(2*x) + 1.0)/(exponential(2*x) - 1.0);
}

#endif
